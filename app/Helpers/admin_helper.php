<?php

use Illuminate\Support\Facades\Auth;
use App\Models\Designations;
use App\Models\User;

if (!function_exists('pr')) {
    function pr($data = array()){
        echo "<pre>";
        print_r($data);
        echo "<pre>";
    }
}

if (!function_exists('userDesignations')) {
    function userDesignations(){
        return Designations::all()->toArray();
    }
}

if (!function_exists('userData')) {
    function userData(){
        return DB::table('users')
        ->select('users.*','user_details.*')
        ->join('user_details','user_details.user_id','=','users.id')
        ->where('users.id', auth()->user()['id'])
        ->get()->first();
    }
}
if (!function_exists('userProfile')) {
    function userProfile(){
        $profile = userData()->profile;
        if (!empty($profile) && file_exists( public_path().'/images/users/'.$profile )) {
            $profilePath = '/images/users/'.$profile;
        }else{
            $profilePath = '/images/user.png';
        }
        return url($profilePath);
    }
}