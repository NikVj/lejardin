<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Calendars;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class CalendarController extends Controller
{

	// 2021-03-08T08:30:00
	// 2021-03-08T13:00:00

	function addCalDetails(Request $request)
	{

		// if (!auth()->user()['is_admin'] == 1) {
		// 	$message = 'Access Denied!';
		// 	return redirect('staff')->withErrors($message);
		// }

		$request->validate([
			'title'       => 'required',
			'etype'       => 'required',
			'start_date'  => 'required',
			'end_date'    => 'required'
		]);

		$startDate = date('Y-m-d'.'\T'.'H:i:s', strtotime($request->all()['start_date']));
		$end_date  = date('Y-m-d'.'\T'.'H:i:s', strtotime($request->all()['end_date']));

		// pr($startDate);

		// pr($request->all());

		$calData = array(
			'user_id'     => auth()->user()['id'],
			'title'       => $request->all()['title'],
			'event_type'       => $request->all()['etype'],
			'start_date'  => $startDate,
			'end_date'    => $end_date,
			'description' => $request->all()['description'],
		);

		// pr($calData);die;

		$addEvent = Calendars::create($calData);

		if ($addEvent) {
			$message = 'Event added successfully!';
			return redirect('app/calendar')->withSuccess($message);
		}else{
			$message = 'Something went wrong!';
			return redirect('app/calendar')->withErrors($message);
		}
	}

	function deleteCalDetails($id)
	{
		$checkCal = Calendars::get()->where('id', $id)->where('user_id', auth()->user()['id'])->first();

        if (auth()->user()['is_admin'] != 1 || empty($checkCal)) {
            return response()->json([
                'class_name'  => 'alert-danger',
                'message'     => 'Something went wrong!'
            ]);
        }else{
            $findId = Calendars::find( $id );
            if ($findId ->delete()){	
                return response()->json([
                    'class_name'  => 'alert-success',
                    'message'     => 'Calendar event deleted successfully!'
                ]);
            }else{   
                return response()->json([
                    'class_name'  => 'alert-danger',
                    'message'     => 'Something went wrong!'
                ]);
            }
        }
	}
}