<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Image;
use File;

class UsersController extends Controller
{

    public function index()
    {
        $allUsers = DB::table('users')
        ->select('users.*','user_details.*')
        ->join('user_details','user_details.user_id','=','users.id')
        ->wherenull('users.deleted_at')
        ->get()->toArray();

        return view('users.staff', compact('allUsers'));
    }

    function saveUserForm($id=NULL)
    {
        if ($id==0) {
            return view('users.staff_save');
        }else{
            $userDetails = User::select('users.*','user_details.*')
            ->join('user_details','user_details.user_id','=','users.id')
            ->where('users.id', $id)
            ->get()->first();

            return view('users.staff_save', compact('userDetails'));
        }
    }

    function saveUserDetails(Request $request)
    {
        if (!auth()->user()['is_admin'] == 1) {
            $message = 'Access Denied!';
            return redirect('staff')->withErrors($message);
        }

        $request->validate([
            'designation' => 'required',
            'name'        => 'required',
            'email'       => 'required|unique:users,email',
            'phone'       => 'required',
            'address1'    => 'required',
            'password'    => 'min:8|required_with:c_password|same:c_password',
        ]);

        $hashedPassword = Hash::make($request->post('password'));
        unset($request['password']);
        $request['password'] = $hashedPassword;

        $addUser = User::create($request->all());

        if ($addUser) {
            $userDetailsData = array(
                'user_id'     => $addUser->id,
                'other_acc'   => request('other_acc'),
                'phone_home'  => request('phone_home'),
                'address1'    => request('address1'),
                'address2'    => request('address2'),
                'status'      => 1,
            );
            DB::table('user_details')->insert($userDetailsData);
        }else{
            $message = 'Something went wrong!';
            return redirect('staff')->withErrors($message);
        }

        $message = 'Staff as "'.$request->post('designation').'" added successfully!';
        return redirect('staff')->withSuccess($message);
    }

    function updateUserDetails(Request $request)
    {
        if (auth()->user()['is_admin'] != 1) {
            $message = 'Access Denied!';
            return redirect('staff')->withErrors($message);
        }

        $userId = $request->all()['id'];
        $request->validate([
            'designation' => 'required',
            'name'        => 'required',
            'email'       => 'required|email|max:225|'. Rule::unique('users')->ignore($userId),
            'phone'       => 'required',
            'address1'    => 'required',
        ]);

        if (!empty($request->post('password'))) {
            $request->validate(['password'    => 'min:8|required_with:c_password|same:c_password']);
            $hashedPassword = Hash::make($request->post('password'));
        }else{
            $hashedPassword = User::where('id',$userId)->first()->password;
        }

        $data = array(
            'designation' => $request->post('designation'),
            'name'        => $request->post('name'),
            'email'       => $request->post('email'),
            'password'    => $hashedPassword,
            'phone'       => $request->post('phone')
        );

        $update = User::wherenull('deleted_at')->where('id',$userId)->first();
        $update->update($data);

        if ($update) {
            $userDetailsData = array(
                'other_acc'   => request('other_acc'),
                'phone_home'  => request('phone_home'),
                'address1'    => request('address1'),
                'address2'    => request('address2')
            );

            DB::table('user_details')->where('id',$update->id)->update($userDetailsData);
        }else{
            $message = 'Something went wrong!';
            return redirect('staff')->withErrors($message);
        }

        $message = 'Staff as "'.$request->post('designation').'" updated successfully!';
        return redirect('staff')->withSuccess($message);
    }

    function deleteUser($id)
    {
        if (auth()->user()['is_admin'] != 1 || $id == auth()->user()['id']) {
            return response()->json([
                'class_name'  => 'alert-danger',
                'message'     => 'Something went wrong!',
                'current'     => '',
                'first'       => ''
            ]);
        }else{
            $findId = User::find( $id );
            if ($findId ->delete()){
                return response()->json([
                    'class_name'  => 'alert-success',
                    'message'     => 'Staff deleted successfully!',
                    'current'     => $id,
                    'first'       => User::first()->id
                ]);
            }else{   
                return response()->json([
                    'class_name'  => 'alert-danger',
                    'message'     => 'Something went wrong!',
                    'current'     => '',
                    'first'       => ''
                ]);
            }
        }
    }

    function uploadImageUser(Request $request, $id)
    {
        if (auth()->user()['is_admin'] != 1 && $id != auth()->user()['id']) {
            return response()->json([
                'class_name'  => 'alert-danger',
                'message'     => 'Something went wrong!',
                'current'     => '',
                'first'       => ''
            ]);
        }else{

            $directoryPath = public_path().'/images/users/';
            if ($directoryPath) {
                File::makeDirectory($directoryPath, $mode = 0777, true, true);
            }

            $findProfile = User::find($id)->profile;
            if (!empty($findProfile && file_exists($directoryPath.$findProfile))) {
                unlink($directoryPath.$findProfile);
            }

            $validation = Validator::make($request->all(), [
                'profile' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
            ]);

            if($validation->passes()){
                $image = $request->file('profile');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $updateField = User::where('id', $id)->update(['profile' => $new_name]);

                if ($updateField) {
                    $resize_image = Image::make($image->getRealPath())->fit(300, 300);
                    $resize_image->save(public_path('images/users/'.$new_name));

                    return response()->json([
                        'class_name'  => 'alert-success',
                        'message'     => 'Image Upload Successfully!',
                        'imagePath'   => '/images/users/'.$new_name
                    ]);
                }
            }else{
                return response()->json([
                    'class_name'  => 'alert-danger',
                    'message'     => $validation->errors()->all(),
                    'imagePath'   => ''
                ]);
            }
        }
    }
}