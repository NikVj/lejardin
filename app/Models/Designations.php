<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Designations extends Model
{
    use HasFactory;

    protected $table="designations";
    public $timestamps= true;
    protected $primaryKey = 'id';

    protected $fillable = [
    	'designation_name',
    	'status',
    ];

}