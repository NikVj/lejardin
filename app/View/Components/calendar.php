<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\DB;
use App\Models\Calendars;

class calendar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        if (auth()->user()['is_admin'] == 1) {
        	$calendarData = Calendars::join('users', 'calendars.user_id', '=', 'users.id')
        					->get(['calendars.id','calendars.event_type','calendars.start_date as start','calendars.end_date as end','calendars.title','calendars.description'])
        					->groupBy('event_type')
        					->toArray();
        }else{
        	$calendarData = Calendars::join('users', 'calendars.user_id', '=', 'users.id')
        					->where('calendars.user_id', auth()->user()->id)
        					->get(['calendars.id','calendars.event_type','calendars.start_date as start','calendars.end_date as end','calendars.title','calendars.description'])
        					->groupBy('event_type')
        					->toArray();
        }

        $data['calEventsData']=$data['bDayEventsData']=$data['holiEventsData']=$data['otherEventsData']='{}';

        if (!empty($calendarData[1])) { $data['calEventsData']   = json_encode($calendarData[1]); }
        if (!empty($calendarData[2])) { $data['bDayEventsData']  = json_encode($calendarData[2]); }
        if (!empty($calendarData[3])) { $data['holiEventsData']  = json_encode($calendarData[3]); }
        if (!empty($calendarData[4])) { $data['otherEventsData'] = json_encode($calendarData[4]); }

        return view('components.calendar', $data);
    }
}
