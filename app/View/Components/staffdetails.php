<?php

namespace App\View\Components;

use Illuminate\View\Component;
use app\Models\User;

class staffdetails extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render($id=NULL)
    {
        if ($id == NULL) {
            $userDetails = User::select('users.*','user_details.*')
            ->join('user_details','user_details.user_id','=','users.id')
            ->get()->first();
        }else{
            $userDetails = User::select('users.*','user_details.*')
            ->join('user_details','user_details.user_id','=','users.id')
            ->where('users.id', $id)
            ->get()->first();
        }
        return view('components.staffdetails', compact('userDetails'));
    }
}