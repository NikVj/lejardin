<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('phone_home')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->integer('zip')->nullable();
            $table->longText('address1')->nullable();
            $table->longText('address2')->nullable();
            $table->string('git_acc')->nullable();
            $table->string('twitter_acc')->nullable();
            $table->string('linkedin_acc')->nullable();
            $table->string('facebook_acc')->nullable();
            $table->string('outlook_acc')->nullable();
            $table->string('instagram_acc')->nullable();
            $table->string('slack_acc')->nullable();
            $table->string('my_acc')->nullable();
            $table->string('other_acc')->nullable();
            $table->string('aadhar_no')->nullable();
            $table->string('website')->nullable();
            $table->string('pan_no')->nullable();
            $table->longText('work')->nullable();
            $table->longText('education')->nullable();
            $table->longText('skill')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
