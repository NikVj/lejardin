<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable();
            $table->string('title');
            $table->integer('calendar_type')->default(1);
            $table->integer('event_type')->default(1);
            $table->longText('description')->nullable();
            $table->tinyInteger('is_public')->default(1);
            $table->tinyInteger('status')->default(1);
            $table->string('start_date');
            $table->string('end_date');
            $table->integer('priority')->default(1);
            $table->string('bgcolor')->default('#d8fed1');
            $table->string('brdcolor')->default('#FF0000');
            $table->softDeletes()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
