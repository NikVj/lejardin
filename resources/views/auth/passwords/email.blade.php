@extends('layouts.app')

@section('content')

<div class="az-signin-wrapper">
    <div class="az-card-signin my-5">
        <div class="az-signin-header mb-2">
            <h5>{{ __('Reset Password') }}</h5>
        </div>

        @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ session('status') }}
        </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-group">
                <label for="email">{{ __('E-Mail Address') }}</label>

                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div>
                <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Send Password Reset Link') }}
                </button>
            </div>
        </form>

        <div class="az-signin-footer mt-3">
            @if (Route::has('password.request'))
            <a class="" href="{{ route('login') }}">
                {{ __("Sign In") }}
            </a>
            @endif
            <a href="{{ route('register') }}" class="float-right">Sign up for an account</a>
        </div>
    </div>
</div>

@endsection