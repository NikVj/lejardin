@extends('layouts.app')
@section('content')

    <link rel="stylesheet" href="{{ asset('assets/lib/lightslider/css/lightslider.min.css')}}">

    <div class="az-content az-content-app pd-b-0">
      <div class="container-fluid">
        <div class="az-content-left az-content-left-chat">

          <nav class="nav az-nav-line az-nav-line-chat">
            <a href="" data-toggle="tab" class="nav-link active">Recent Chat</a>
            <a href="" data-toggle="tab" class="nav-link">Groups</a>
            <a href="" data-toggle="tab" class="nav-link">Calls</a>
          </nav>

          <div class="az-chat-contacts-wrapper">
            <label class="az-content-label az-content-label-sm">Active Contacts (5)</label>
            <div id="chatActiveContacts" class="az-chat-contacts">
              <div>
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <small>Adrian</small>
              </div>
              <div>
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <small>John</small>
              </div>
              <div>
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <small>Daniel</small>
              </div>
              <div>
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <small>Katherine</small>
              </div>
              <div>
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <small>Raymart</small>
              </div>
              <div>
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <small>Junrisk</small>
              </div>
              <div>
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <small>George</small>
              </div>
              <div>
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <small>Maryjane</small>
              </div>
              <div>
                <div class="az-chat-contacts-more">20+</div>
                <small>More</small>
              </div>
            </div><!-- az-active-contacts -->
          </div><!-- az-chat-active-contacts -->

          <div id="azChatList" class="az-chat-list">
            <div class="media new">
              <div class="az-img-user online">
                <img src="{{BASE_PATH.'/images/user.png'}}">
                <span>2</span>
              </div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Socrates Itumay</span>
                  <span>2 hours</span>
                </div>
                <p>Nam quam nunc, blandit vel aecenas et ante tincid</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media new">
              <div class="az-img-user">
                <img src="{{BASE_PATH.'/images/user.png'}}">
                <span>1</span>
              </div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Dexter dela Cruz</span>
                  <span>3 hours</span>
                </div>
                <p>Maec enas tempus, tellus eget con dime ntum rhoncus, sem quam</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media selected">
              <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Reynante Labares</span>
                  <span>10 hours</span>
                </div>
                <p>Nam quam nunc, bl ndit vel aecenas et ante tincid</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media">
              <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Joyce Chua</span>
                  <span>2 days</span>
                </div>
                <p>Ma ecenas tempus, tellus eget con dimen tum rhoncus, sem quam</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media">
              <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Rolando Paloso</span>
                  <span>2 days</span>
                </div>
                <p>Nam quam nunc, blandit vel aecenas et ante tincid</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media new">
              <div class="az-img-user">
                <img src="{{BASE_PATH.'/images/user.png'}}">
                <span>1</span>
              </div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Ariana Monino</span>
                  <span>3 days</span>
                </div>
                <p>Maece nas tempus, tellus eget cond imentum rhoncus, sem quam</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media">
              <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Maricel Villalon</span>
                  <span>4 days</span>
                </div>
                <p>Nam quam nunc, blandit vel aecenas et ante tincid</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media">
              <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Maryjane Pechon</span>
                  <span>5 days</span>
                </div>
                <p>Mae cenas tempus, tellus eget co ndimen tum rhoncus, sem quam</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media">
              <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Lovely Dela Cruz</span>
                  <span>5 days</span>
                </div>
                <p>Mae cenas tempus, tellus eget co ndimen tum rhoncus, sem quam</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media">
              <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Daniel Padilla</span>
                  <span>5 days</span>
                </div>
                <p>Mae cenas tempus, tellus eget co ndimen tum rhoncus, sem quam</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media">
              <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>John Pratts</span>
                  <span>6 days</span>
                </div>
                <p>Mae cenas tempus, tellus eget co ndimen tum rhoncus, sem quam</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media">
              <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Raymart Santiago</span>
                  <span>6 days</span>
                </div>
                <p>Nam quam nunc, blandit vel aecenas et ante tincid</p>
              </div><!-- media-body -->
            </div><!-- media -->
            <div class="media">
              <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
              <div class="media-body">
                <div class="media-contact-name">
                  <span>Samuel Lerin</span>
                  <span>7 days</span>
                </div>
                <p>Nam quam nunc, blandit vel aecenas et ante tincid</p>
              </div><!-- media-body -->
            </div><!-- media -->
          </div><!-- az-chat-list -->

        </div><!-- az-content-left -->
        <div class="az-content-body az-content-body-chat">
          <div class="az-chat-header">
            <div class="az-img-user"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
            <div class="az-chat-msg-name">
              <h6>Reynante Labares</h6>
              <small>Last seen: 2 minutes ago</small>
            </div>
            <nav class="nav">
              <a href="" class="nav-link"><i class="icon ion-md-more"></i></a>
              <a href="" class="nav-link" data-toggle="tooltip" title="Call"><i class="icon ion-ios-call"></i></a>
              <a href="" class="nav-link" data-toggle="tooltip" title="Archive"><i class="icon ion-ios-filing"></i></a>
              <a href="" class="nav-link" data-toggle="tooltip" title="Trash"><i class="icon ion-md-trash"></i></a>
              <a href="" class="nav-link" data-toggle="tooltip" title="View Info"><i class="icon ion-md-information-circle"></i></a>
            </nav>
          </div><!-- az-chat-header -->
          <div id="azChatBody" class="az-chat-body">
            <div class="content-inner">
              <label class="az-chat-time"><span>3 days ago</span></label>
              <div class="media flex-row-reverse">
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <div class="media-body">
                  <div class="az-msg-wrapper">
                    Nulla consequat massa quis enim. Donec pede justo, fringilla vel...
                  </div><!-- az-msg-wrapper -->
                  <div class="az-msg-wrapper">rhoncus ut, imperdiet a, venenatis vitae, justo...</div>
                  <div class="az-msg-wrapper pd-0">
                    <img src="https://via.placeholder.com/500x334" class="wd-200" alt="">
                  </div><!-- az-msg-wrapper -->
                  <div><span>9:48 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a></div>
                </div><!-- media-body -->
              </div><!-- media -->
              <div class="media">
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <div class="media-body">
                  <div class="az-msg-wrapper">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                  </div><!-- az-msg-wrapper -->
                  <div><span>9:32 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a></div>
                </div><!-- media-body -->
              </div><!-- media -->
              <div class="media flex-row-reverse">
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <div class="media-body">
                  <div class="az-msg-wrapper">
                    Nullam dictum felis eu pede mollis pretium
                  </div><!-- az-msg-wrapper -->
                  <div><span>11:22 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a></div>
                </div><!-- media-body -->
              </div><!-- media -->
              <label class="az-chat-time"><span>Yesterday</span></label>
              <div class="media">
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <div class="media-body">
                  <div class="az-msg-wrapper">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                  </div><!-- az-msg-wrapper -->
                  <div><span>9:32 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a></div>
                </div><!-- media-body -->
              </div><!-- media -->
              <div class="media flex-row-reverse">
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <div class="media-body">
                  <div class="az-msg-wrapper">
                    Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                  </div><!-- az-msg-wrapper -->
                  <div class="az-msg-wrapper">
                    Nullam dictum felis eu pede mollis pretium
                  </div><!-- az-msg-wrapper -->
                  <div><span>9:48 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a></div>
                </div><!-- media-body -->
              </div><!-- media -->

              <label class="az-chat-time"><span>Today</span></label>
              <div class="media">
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <div class="media-body">
                  <div class="az-msg-wrapper">
                    Maecenas tempus, tellus eget condimentum rhoncus
                  </div><!-- az-msg-wrapper -->
                  <div class="az-msg-wrapper">
                    Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus.
                  </div><!-- az-msg-wrapper -->
                  <div><span>10:12 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a></div>
                </div><!-- media-body -->
              </div><!-- media -->
              <div class="media flex-row-reverse">
                <div class="az-img-user online"><img src="{{BASE_PATH.'/images/user.png'}}"></div>
                <div class="media-body">
                  <div class="az-msg-wrapper">
                    Maecenas tempus, tellus eget condimentum rhoncus
                  </div><!-- az-msg-wrapper -->
                  <div class="az-msg-wrapper">
                    Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus.
                  </div><!-- az-msg-wrapper -->
                  <div><span>09:40 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a></div>
                </div><!-- media-body -->
              </div><!-- media -->
            </div><!-- content-inner -->
          </div><!-- az-chat-body -->
          <div class="az-chat-footer">
            <nav class="nav">
              <a href="" class="nav-link" data-toggle="tooltip" title="Add Photo"><i class="fas fa-camera"></i></a>
              <a href="" class="nav-link" data-toggle="tooltip" title="Attach a File"><i class="fas fa-paperclip"></i></a>
              <a href="" class="nav-link" data-toggle="tooltip" title="Add Emoticons"><i class="far fa-smile"></i></a>
              <a href="" class="nav-link"><i class="fas fa-ellipsis-v"></i></a>
            </nav>
            <input type="text" class="form-control" placeholder="Type your message here...">
            <a href="" class="az-msg-send"><i class="far fa-paper-plane"></i></a>
          </div><!-- az-chat-footer -->
        </div><!-- az-content-body -->
      </div>
    </div>


    

    <script src="{{ asset('assets/lib/lightslider/js/lightslider.min.js')}}"></script>
    <script>
      $(function(){
        'use strict'

        $('#chatActiveContacts').lightSlider({
          autoWidth: true,
          controls: false,
          pager: false,
          slideMargin: 12
        });

        if(window.matchMedia('(min-width: 992px)').matches) {
          new PerfectScrollbar('#azChatList', {
            suppressScrollX: true
          });

          const azChatBody = new PerfectScrollbar('#azChatBody', {
            suppressScrollX: true
          });

          $('#azChatBody').scrollTop($('#azChatBody').prop('scrollHeight'));
        }


        $('.az-chat-list .media').on('click touch', function() {
          $(this).addClass('selected').removeClass('new');
          $(this).siblings().removeClass('selected');

          if(window.matchMedia('(max-width: 991px)').matches) {
            $('body').addClass('az-content-body-show');
            $('html body').scrollTop($('html body').prop('scrollHeight'));
          }
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('#azChatBodyHide').on('click touch', function(e){
          e.preventDefault();
          $('body').removeClass('az-content-body-show');
        })

      });
    </script>

@endsection