<link href="{{ asset('assets/lib/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/lib/select2/css/select2.min.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('assets/lib/amazeui-datetimepicker/css/amazeui.datetimepicker.css') }}" rel="stylesheet"> -->

<div class="az-content az-content-calendar" id="myCalendar">
  @if(session('success'))
  <div class="alert alert-success mg-b-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>{{session('success')}}</strong>
  </div>
  @endif

  @if ($errors->any())
  <div class="alert alert-danger mb-0">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>Error!</strong> 
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
  @endif

  <div class="container-fluid">
    <div class="az-content-left az-content-left-calendar pt-2">

      <div id="dateToday" class="az-content-label az-content-label-sm tx-medium lh-1 mg-b-10"></div>
      <h2 class="az-content-title mg-b-25 tx-24">My Calendar</h2>

      <div class="fc-datepicker az-datepicker mg-b-30"></div>

      <label class="az-content-label tx-13 tx-bold mg-b-10">Event List</label>
      <nav class="nav az-nav-column az-nav-calendar-event">
          <a href="" class="nav-link @if($calEventsData == '{}'){{'disabled text-secondary'}}@endif">
            <i class="icon ion-ios-calendar tx-primary"></i>
            <div>Calendar Events</div></a>
          <a href="" class="nav-link @if($bDayEventsData == '{}'){{'disabled text-secondary'}}@endif">
            <i class="icon ion-ios-calendar tx-success"></i>
            <div>Birthday Events</div></a>
          <a href="" class="nav-link @if($holiEventsData == '{}'){{'disabled text-secondary'}}@endif">
            <i class="icon ion-ios-calendar tx-danger"></i>
            <div>Holiday Calendar</div></a>
          <a href="" class="nav-link @if($otherEventsData == '{}'){{'disabled text-secondary'}}@endif">
            <i class="icon ion-ios-calendar tx-warning"></i>
            <div>Other Calendar</div></a>
      </nav>
    </div>
    <div class="az-content-body az-content-body-calendar">

      <div id="calendar" class="az-calendar"></div>
    </div><!-- az-content-body -->
  </div><!-- container -->
</div><!-- az-content -->

<div class="modal az-modal-calendar-schedule" id="modalSetSchedule" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title">Create New Event</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div><!-- modal-header -->
      <div class="modal-body">
        <form method="post" action="addCalDetails" id="azFormCalendar">
          {{@csrf_field()}}
          <div class="form-group">
            <input type="text" name="title" class="form-control" placeholder="Add title">
          </div><!-- form-group -->

          <div class="form-group d-flex align-items-center">
            <label class="rdiobox mg-r-30">
              <input type="radio" name="etype" value="1" checked>
              <span>Event</span>
            </label>
            <label class="rdiobox mg-r-30">
              <input type="radio" name="etype" value="2" checked>
              <span>Birthday</span>
            </label>
            <label class="rdiobox mg-r-30">
              <input type="radio" name="etype" value="3" checked>
              <span>Holiday</span>
            </label>
            <label class="rdiobox">
              <input type="radio" name="etype" value="4">
              <span>Other</span>
            </label>
          </div><!-- form-group -->

          <div class="form-group mg-t-30">
            <label class="tx-13 mg-b-5 tx-gray-600">Start Event</label>
            <div class="row row-xs">
              <div class="col-12">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                    </div>
                  </div>
                  <input type="text" name="start_date" id="azEventStartDateOLD" value="{{date('Y-m-d')}} 12:00" class="form-control dtimepicker">
                </div>
              </div><!-- col-7 -->

            </div><!-- row -->
          </div><!-- form-group -->
          <div class="form-group">
            <label class="tx-13 mg-b-5 tx-gray-600">End Event</label>
            <div class="row row-xs">
              <div class="col-12">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                    </div>
                  </div>
                  <input type="text" name="end_date" id="azEventEndDateOLD" value="{{date('Y-m-d')}} 12:00" class="form-control dtimepicker">
                </div>
              </div>
            </div><!-- row -->
          </div><!-- form-group -->
          <div class="form-group">
            <textarea class="form-control" name="description" rows="2" placeholder="Write some description (optional)"></textarea>
          </div><!-- form-group -->

          <div class="d-flex mg-t-15 mg-lg-t-30">
            <button type="submit" class="btn btn-az-primary pd-x-25 mg-r-5">Save</button>
            <a href="" class="btn btn-light" data-dismiss="modal">Discard</a>
          </div>
        </form>
      </div><!-- modal-body -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->

<div class="modal az-modal-calendar-event" id="modalCalendarEvent" data-id="" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="event-title"></h6>
        <nav class="nav nav-modal-event">
          <a href="#" class="nav-link"><i class="icon ion-md-open"></i></a>
          <a href="javascript:;" class="nav-link" id="dltCalBtn"><i class="icon ion-md-trash"></i></a>
          <a href="#" class="nav-link" data-dismiss="modal"><i class="icon ion-md-close"></i></a>
        </nav>
      </div><!-- modal-header -->
      <div class="modal-body">
        <div class="row row-sm">
          <div class="col-sm-6">
            <label class="tx-13 tx-gray-600 mg-b-2">Start Date</label>
            <p class="event-start-date"></p>
          </div>
          <div class="col-sm-6">
            <label class="tx-13 mg-b-2">End Date</label>
            <p class="event-end-date"></p>
          </div><!-- col-6 -->
        </div><!-- row -->

        <label class="tx-13 tx-gray-600 mg-b-2">Description</label>
        <p class="event-desc tx-gray-900 mg-b-30"></p>

        <a href="" class="btn btn-secondary wd-80" data-dismiss="modal">Close</a>
      </div><!-- modal-body -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->

<script src="{{ asset('assets/lib/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('assets/lib/jquery-ui/ui/widgets/datepicker.js') }}"></script>
<script src="{{ asset('assets/lib/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('assets/lib/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/js/app-calendar.js') }}"></script>
<script src="{{ asset('assets/lib/amazeui-datetimepicker/js/amazeui.datetimepicker.min.js') }}"></script>

<script>
  $(function(){
    'use strict'

    $('.select2-modal').select2({
      minimumResultsForSearch: Infinity,
      dropdownCssClass: 'az-select2-dropdown-modal',
    });

    $('#dateToday').text(moment().format('ddd, MMMM DD YYYY'));

    // AmazeUI Datetimepicker
    $('.dtimepicker').datetimepicker({
      format: 'yyyy-mm-dd hh:ii',
      autoclose: true
    });

  });
</script>

<script type="text/javascript">
  'use strict'

  var cEvents     = <?= $calEventsData ?>;
  var bDayEvents  = <?= $bDayEventsData ?>;
  var holiEvents  = <?= $holiEventsData ?>;
  var otherEvents = <?= $otherEventsData ?>;

  var curYear  = moment().format('YYYY');
  var curMonth = moment().format('MM');

  var azCalendarEvents = {
    id: 1,
    backgroundColor: '#bff2f2',
    borderColor: '#00cccc',
    events: cEvents
  };

  var azBirthdayEvents = {
    id: 2,
    backgroundColor: '#cbfbb0',
    borderColor: '#3bb001',
    events: bDayEvents
  };

  var azHolidayEvents = {
    id: 3,
    backgroundColor: '#fbbfdc',
    borderColor: '#f10075',
    events: holiEvents
  };

  var azOtherEvents = {
    id: 4,
    backgroundColor: '#ffecca',
    borderColor: '#ffb52b',
    events: otherEvents
  };
</script>

<script type="text/javascript">
  $('#dltCalBtn').click(function(){
    var ID = $('#modalCalendarEvent').attr('data-id');

    $.ajax({
      url: "{{'deleteCalendar'}}" +'/'+ ID,
      success: function (data){
        if (data.class_name == 'alert-success') {
          $('[data-act=removed]').slideToggle();
        }
        $('.alert').hide(200);
        var html='<div class="alert '+ data.class_name +' mg-b-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+ data.message +'</strong></div>';
        $( ".az-content-calendar" ).prepend($(html));
        $('#modalCalendarEvent').modal('toggle');
      }
    });
  });
</script>