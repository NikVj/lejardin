
<?php
	// pr(userProfile());
?>

<div class="az-header az-header-dashboard-nine">
	<div class="container-fluid">
		<div class="az-header-left">
			<a href="" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
		</div>
		<div class="header-search-wrapper">
			<input type="search" class="header-search-input z-depth-2" placeholder="Search...">
			<button class="btn"><i class="fas fa-search"></i></button>
		</div>
		<div class="az-header-right">
			<div class="az-header-message">
				<a href="{{ url('app/messaging') }}"><i class="typcn typcn-messages"></i></a>
			</div>
			<div class="dropdown az-header-notification">
				<a href="javascript:;" class="new"><i class="typcn typcn-bell"></i></a>
				<div class="dropdown-menu">
					<div class="az-dropdown-header mg-b-20 d-sm-none">
						<a href="" class="az-header-arrow"><i class="icon ion-md-arrow-back"></i></a>
					</div>
					<h6 class="az-notification-title">Notifications</h6>
					<p class="az-notification-text">You have 2 unread notification</p>
					<div class="az-notification-list">
						<div class="media new">
							<div class="az-img-user"><img src="{{userProfile()}}"></div>
							<div class="media-body">
								<p>Congratulate <strong>Socrates Itumay</strong> for work anniversaries</p>
								<span>Mar 15 12:32pm</span>
							</div>
						</div>
						<div class="media new">
							<div class="az-img-user online"><img src="{{userProfile()}}"></div>
							<div class="media-body">
								<p><strong>Joyce Chua</strong> just created a new blog post</p>
								<span>Mar 13 04:16am</span>
							</div>
						</div>
						<div class="media">
							<div class="az-img-user"><img src="{{userProfile()}}"></div>
							<div class="media-body">
								<p><strong>Althea Cabardo</strong> just created a new blog post</p>
								<span>Mar 13 02:56am</span>
							</div>
						</div>
						<div class="media">
							<div class="az-img-user"><img src="{{userProfile()}}"></div>
							<div class="media-body">
								<p><strong>Adrian Monino</strong> added new comment on your photo</p>
								<span>Mar 12 10:40pm</span>
							</div>
						</div>
					</div>
					<div class="dropdown-footer"><a href="">View All Notifications</a></div>
				</div>
			</div>
			<div class="dropdown az-profile-menu">
				<a href="" class="az-img-user"><img src="{{userProfile()}}"></a>
				<div class="dropdown-menu">
					<div class="az-dropdown-header d-sm-none">
						<a href="" class="az-header-arrow"><i class="icon ion-md-arrow-back"></i></a>
					</div>
					<div class="az-header-profile">
						<div class="az-img-user-profile az-img-user">
							<img src="{{userProfile()}}">
						</div>
						<h6>{{userData()->name}}</h6>
						<span>{{userData()->email}}</span>
					</div>
					<a href="{{ url('profile') }}" class="dropdown-item"><i class="typcn typcn-user-outline"></i> My Profile</a>
					<a href="" class="dropdown-item"><i class="typcn typcn-time"></i> Activity Logs</a>
					<a href="" class="dropdown-item"><i class="typcn typcn-cog-outline"></i> Account Settings</a>
					<form method="POST" action="logout">
						@csrf
						<a href="javascript:;" class="dropdown-item" onclick="event.preventDefault();
						this.closest('form').submit();"><i class="typcn typcn-power-outline"></i> Sign Out</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>