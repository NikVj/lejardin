<div class="az-sidebar az-sidebar-sticky az-sidebar-indigo-dark">
	<div class="az-sidebar-header">
		<a href="{{ url('/') }}" class="az-logo">{{SITE_NAME}}</a>
	</div>

	<div class="aside-topper">
		<!-- <div class="az-sidebar-loggedin pos-relative" onmouseout="$('#asideToolsBtn').show(100);" onmouseleave="$('#asideToolsBtn').hide(100);">
			<div class="az-img-user online"><img src="{{userProfile()}}"></div>
			<div class="media-body">
				<h6>{{userData()->name}}</h6>
				<span>Administrator</span>
				<span>{{userData()->email}}</span>
			</div>
			<i class="fas fa-angle-down pos-absolute tx-20 rounded-50 pd-y-2 pd-x-6 r-100 bg-white" id="asideToolsBtn" onclick="$('#asideTools').slideToggle()"></i>
		</div> -->
		<div class="materializedparent">
			<ul id="slide-out" class="side-nav fixed leftside-navigation">
	                <li class="user-details cyan darken-2">
	                <div class="row">
	                    <div class="col col s4 m4 l4">
	                        <img src="{{userProfile()}}" alt="" class="circle responsive-img valign profile-image">
	                    </div>
	                    <div class="col col s8 m8 l8">
		                    <div class="dropdown">
							  <a class="dropdown-toggle waves-effect waves-light white-text profile-btn btn-flat" data-toggle="dropdown">John Doe
							  <span class="caret"></span></a>
							  <ul class="dropdown-menu">
							    <li><a href="#"><i class="fa fa-user-o"></i> Profile</a></li>
							    <li><a href="#"><i class="fa fa-sliders"></i> Settings</a></li>
							    <li><a href="#"><i class="fas fa-lock mr-2"></i> Lock</a></li>
							    <li><a href="#"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
							  </ul>
							</div>
						</div>
	                </div>
	            </li>
	        </ul>
	    </div>

		<div class="pd-x-10 pd-y-10 bg-gray-200" id="asideTools">
			<div class="media-body">
				<i class="fas fa-lock mr-2 icon-btn"></i>

				<form method="POST" action="logout" class="d-inline">
					@csrf
					<i class="fas fa-sign-out-alt mr-2 icon-btn" onclick="event.preventDefault();
					this.closest('form').submit();"></i>
				</form>

			</div>
		</div>
	</div>

	<div class="az-sidebar-body">
		<ul class="nav">

			<li class="nav-item {{ (request()->is('home')) ? 'active' : '' }}">
				<a href="{{ url('home') }}" class="nav-link">
					<i class="fas fa-home"></i>
					Dashboard
				</a>
			</li>

			<li class="nav-item {{ (request()->is('staff')) ? 'active' : '' }}">
				<a href="{{ url('staff') }}" class="nav-link">
					<i class="fas fa-users"></i>
					Staff
				</a>
			</li>

			<li class="nav-item {{ (request()->is('app*')) ? 'active show' : '' }}">
				<a href="" class="nav-link with-sub"><i class="fas fa-box-open"></i>Applications</a>
				<ul class="nav-sub">
					<li class="nav-sub-item {{ (request()->is('app/calendar')) ? 'active' : '' }}">
						<a href="{{ url('app/calendar') }}" class="nav-sub-link"><i class="fas fa-calendar-alt"></i>Calendar</a>
					</li>
					<li class="nav-sub-item {{ (request()->is('app/messaging')) ? 'active' : '' }}">
						<a href="{{ url('app/messaging') }}" class="nav-sub-link"><i class="fas fa-comment-alt"></i>Chat</a>
					</li>
					<li class="nav-sub-item {{ (request()->is('app/mail')) ? 'active' : '' }}">
						<a href="{{ url('app/mail') }}" class="nav-sub-link"><i class="fa fa-envelope"></i>Mail</a>
					</li>
				</ul>
			</li>
			<li class="nav-item {{ (request()->is('home')) ? 'active' : '' }}">
				<a href="{{ url('home') }}" class="nav-link">
					<i class="fas fa-home"></i>
					Dashboard
				</a>
			</li>
			<li class="nav-item {{ (request()->is('home')) ? 'active' : '' }}">
				<a href="{{ url('home') }}" class="nav-link">
					<i class="fas fa-home"></i>
					Dashboard
				</a>
			</li>
			<li class="nav-item {{ (request()->is('home')) ? 'active' : '' }}">
				<a href="{{ url('home') }}" class="nav-link">
					<i class="fas fa-home"></i>
					Dashboard
				</a>
			</li>
			<li class="nav-item {{ (request()->is('home')) ? 'active' : '' }}">
				<a href="{{ url('home') }}" class="nav-link">
					<i class="fas fa-home"></i>
					Dashboard
				</a>
			</li>
			<li class="nav-item {{ (request()->is('home')) ? 'active' : '' }}">
				<a href="{{ url('home') }}" class="nav-link">
					<i class="fas fa-home"></i>
					Dashboard
				</a>
			</li>
			<li class="nav-item {{ (request()->is('home')) ? 'active' : '' }}">
				<a href="{{ url('home') }}" class="nav-link">
					<i class="fas fa-home"></i>
					Dashboard
				</a>
			</li>
		</ul>
	</div>
</div>