<?php
  $workingExp; $educations; $skills;
  if (!empty($userDetails->work)){ $workingExp = json_decode($userDetails->work); }
  if (!empty($userDetails->education)){ $educations = json_decode($userDetails->education); }
  if (!empty($userDetails->skill)){ $skills     = json_decode($userDetails->skill); }
?>

  <div class="az-content-body az-content-body-contacts">
  <div class="az-contact-info-header">
    <div class="media">
      <div class="az-img-user">
        <div class="alert" id="message" style="display: none"></div>
        <form method="post" id="profileUploadForm" enctype="multipart/form-data">
          @csrf
          <div class="az-img-user-profile">
            @if(!empty($userDetails->profile) && file_exists( public_path().'/images/users/'.$userDetails->profile ))
              <img src="{{'images/users/'.$userDetails->profile}}" id="uploaded_image">
            @else
              <img src="{{'images/user.png'}}" id="uploaded_image">
            @endif

            @if($userDetails->id == auth()->user()['id'] || auth()->user()['is_admin'] == 1)
              <a href="javascript:;" onclick="$('#userProfileInput').trigger('click');">
                <i class="typcn typcn-camera-outline"></i>
              </a>
              <input type='file' name='profile' id='userProfileInput' data-id="{{$userDetails->id}}" class="d-none" accept="image/*">
            @endif
          </div>
        </form>
      </div>
      <div class="media-body">
        <h4>@if(!empty($userDetails->name)){{$userDetails->name}}@endif</h4>
        <p>@if(!empty($userDetails->designation)){{$userDetails->designation}}@endif</p>
        <nav class="nav">
          <a href="" class="nav-link"><i class="typcn typcn-device-phone"></i> Call</a>
          <a href="" class="nav-link"><i class="typcn typcn-messages"></i> Message</a>
          <a href="" class="nav-link"><i class="typcn typcn-user-add-outline"></i> Add to Group</a>
          <a href="" class="nav-link"><i class="typcn typcn-cancel"></i> Block</a>
        </nav>
      </div>
    </div>
    <div class="az-contact-action">
      @if(auth()->user()['is_admin'] == 1)
      <a href="javascript:;" onclick="loadPage('loadedPage','{{$userDetails->id}}','{{ url('saveuserform') }}');"><i class="typcn typcn-edit"></i> Edit Staff</a>
      @endif

      @if($userDetails->id != auth()->user()['id'] && auth()->user()['is_admin'] == 1)
      <a href="javascript:;" id="deleteUserBtn" data-toggle="modal" data-target="#confirmDeleteModal" data-id="{{$userDetails->id}}">
        <i class="typcn typcn-trash"></i> Delete Staff
      </a>
      @endif
    </div>
  </div>

  <div class="az-contact-info-body">
    <div class="media-list">
      <div class="media">
        <div class="media-icon"><i class="fas fa-mobile-alt"></i></div>
        <div class="media-body">
          <div>
            <label>Work</label>
            <span class="tx-medium">@if(!empty($userDetails->phone)){{$userDetails->phone}}@endif</span>
          </div>
          <div>
            <label>Personal</label>
            <span class="tx-medium">@if(!empty($userDetails->phone_home)){{$userDetails->phone_home}}@endif</span>
          </div>
        </div>
      </div>
      <div class="media">
        <div class="media-icon align-self-start"><i class="far fa-envelope"></i></div>
        <div class="media-body">
          <div>
            <label>Gmail Account</label>
            <span class="tx-medium">@if(!empty($userDetails->email)){{$userDetails->email}}@endif</span>
          </div>
          <div>
            <label>Other Account</label>
            <span class="tx-medium">@if(!empty($userDetails->other_acc)){{$userDetails->other_acc}}@endif</span>
          </div>
        </div>
      </div>
      <div class="media">
        <div class="media-icon"><i class="far fa-map"></i></div>
        <div class="media-body"> 
          <div>
            <label>Current Address</label>
            <span class="tx-medium">@if(!empty($userDetails->address1)){{$userDetails->address1}}@endif</span>
          </div>
        </div>
      </div>
      <div class="media">
        <div class="media-icon"><i class="far fa-map"></i></div>
        <div class="media-body">
          <div>
            <label>Permanent Address</label>
            <span class="tx-medium">@if(!empty($userDetails->address2)){{$userDetails->address2}}@endif</span>
          </div>
        </div>
      </div>

      <div class="media">
        <div class="media-icon"><i class="fas fa-less-than"></i></div>
        <div class="media-body">
          <div>
            <div class="az-content-label tx-13 mg-b-20">Skill Sets</div>
            @if (!empty($skills))
            @foreach ($skills as $skill)
            <div class="az-traffic-detail-item">
              <div>
                <span>{{$skill->name}}</span>
                <span>{{$skill->number}}%</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="{{$skill->number}}" aria-valuemin="0" aria-valuemax="100" style="background-color:{{$skill->bgcolor}}; width: {{$skill->number}}%"></div>
              </div>
            </div>
            @endforeach
            @else
            <h2 class="text-secondary">Not Available</h2>
            @endif
          </div>
        </div>
      </div>

      <div class="media">
        <div class="media-icon"><i class="fas fa-graduation-cap"></i></div>
        <div class="media-body">
          <div>
            <div class="az-content-label tx-13 mg-b-20">Education</div>
            @if (!empty($educations))
            @foreach ($educations as $education)
            <div class="mb-3">
              <h6 class="mb-0">{{$education->name}} from <a href="javascript:;">{{$education->university}}</a></h6>
              <span>{{$education->from}} - {{$education->to}}</span>
            </div>
            @endforeach
            @else
            <h2 class="text-secondary">Not Available</h2>
            @endif
          </div>

          <div>
            <div class="az-content-label tx-13 mg-b-20">Work</div>
            @if (!empty($workingExp))
            @foreach ($workingExp as $work)
            <div class="mb-3">
              <h6 class="mb-0">{{$work->designation}} at <a href="{{$work->website}}" target="_blank">{{$work->organization}}</a></h6>
              <span>{{$work->from}} - {{$work->to}}</span>
            </div>
            @endforeach
            @else
            <h2 class="text-secondary">Not Available</h2>
            @endif
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div id="confirmDeleteModal" class="modal" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-content-demo">
      <div class="modal-header">
        <h6 class="modal-title">Warning</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <h6 class="mb-0">Are you sure you want to delete this account?</h6>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-light" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-danger" id="confirmDeleteButton">Yes, Delete!</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#deleteUserBtn').click(function(){
    var ID = $(this).data('id');
    $('#confirmDeleteButton').attr('data-id', ID);
  });

  $('#confirmDeleteButton').click(function(){
    var ID = $(this).data('id');
    $.ajax({
      url: "{{'deleteUser'}}" +'/'+ ID,
      success: function (data){
        if (data.current != '' && data.first != '') {
          $('#azContactList [data-id='+data.current+']').hide();
          loadPage('loadedPage',data.first,'{{ url('staff_details') }}');
        }
        $('.alert').hide(200);
        var html='<div class="alert '+ data.class_name +' mg-b-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+ data.message +'</strong></div>';
        $( ".az-content-contacts" ).prepend($(html));
        $('#confirmDeleteModal').modal('toggle');
      }
    });
  });

  $(document).ready(function(){
    $('#userProfileInput').change(function(){
      event.preventDefault();
      $.ajax({
        url:"{{'uploadImageUser'}}"+"/{{$userDetails->id}}",
        method:"POST",
        data:new FormData(document.getElementById("profileUploadForm")),
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success:function(data){
          $('.alert').hide(200);
          var html='<div class="alert '+ data.class_name +' mg-b-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+ data.message +'</strong></div>';
          $( ".az-content-contacts" ).prepend($(html));
          $("#uploaded_image").attr("src",data.imagePath);
        }
      })
    });
  });
</script>