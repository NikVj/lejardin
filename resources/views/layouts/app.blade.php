<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="{{ asset('assets/lib/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/lib/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/lib/typicons.font/typicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/lib/jqvmap/jqvmap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/office.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- CORE CSS-->    
    <!-- <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection"> -->
    <link href="{{ asset('css/custom_materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="{{ asset('css/custom/custom-style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('js/plugins/jvectormap/jquery-jvectormap.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('js/plugins/chartist-js/chartist.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">


    <script src="{{ asset('assets/lib/jquery/jquery.min.js') }}"></script>
</head>

<body class="az-body az-body-sidebar az-body-dashboard-nine">

    <main>
        @guest
            <div>
                @yield('content')
            </div>
        @else
            <x-sidebar />
            <div class="az-content az-content-dashboard-nine">
                <x-header />
                @yield('content')
                <x-footer />
            </div>
        @endguest
    </main>

    <script src="{{ asset('assets/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/lib/ionicons/ionicons.js') }}"></script>
    <script src="{{ asset('assets/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/js/office.js') }}"></script>
  
    <!--materialize js-->
    <script type="text/javascript" src="{{ asset('js/materialize.js') }}"></script>
</body>
</html>