@extends('layouts.app')
@section('content')

<?php
  $workingExp; $educations; $skills;
  if (!empty(userData()->work)){ $workingExp = json_decode(userData()->work); }
  if (!empty(userData()->education)){ $educations = json_decode(userData()->education); }
  if (!empty(userData()->skill)){ $skills     = json_decode(userData()->skill); }
?>

<div id="loadedPage" style="width: 100%"></div>

<div class="az-content az-content-profile">
  <div class="container-fluid mn-ht-100p">
    <div class="az-content-left az-content-left-profile col-lg-3">
      
      @if(auth()->user()['id'] == userData()->id)
      <div class="btn-action mt-2 mr-2">
        <a href="javascript:;" class="text-info text-right ml-2 tx-20" onclick="loadPage('loadedPage','{{(userData()->id)}}','{{ url('profileform') }}');$('.az-content-profile').hide();"><i class="fa fa-edit"></i></a>
      </div>
      @endif

      <div class="az-profile-overview">
      	<form method="post" id="profileUploadForm" enctype="multipart/form-data">
      		@csrf
      		<div class="az-contact-info-header pd-l-0">
      			<div class="media">
      				<div class="az-img-user">
      					<img src="{{userProfile()}}" id="uploaded_image">

      					<a href="javascript:;" onclick="$('#userProfileInput').trigger('click');">
      						<i class="typcn typcn-camera-outline"></i>
      					</a>
      					<input type="file" name="profile" id="userProfileInput" data-id="1" class="d-none" accept="image/*">
      				</div>
      			</div>
      		</div>
      	</form>

        <div class="d-flex justify-content-between mg-b-10">
          <div>
            <h5 class="az-profile-name">{{userData()->name}}</h5>
            <p class="az-profile-name-text">{{userData()->designation}}</p>
          </div>
          <div class="btn-icon-list">
            <button class="btn btn-indigo btn-icon"><i class="typcn typcn-plus-outline"></i></button>
            <button class="btn btn-primary btn-icon"><i class="typcn typcn-message"></i></button>
          </div>
        </div>

        <div class="az-profile-bio">{{userData()->email}}
        </div><!-- az-profile-bio -->

        <hr class="mg-y-30">

        <label class="az-content-label tx-13 mg-b-20">Websites &amp; Social Channel</label>
        <div class="az-profile-social-list">
          <div class="media">
            <div class="media-icon"><i class="icon ion-logo-github"></i></div>
            <div class="media-body">
              <span>Github</span>
              <a href="javascript:;">{{userData()->git_acc}}</a>
            </div>
          </div><!-- media -->
          <div class="media">
            <div class="media-icon"><i class="icon ion-logo-twitter"></i></div>
            <div class="media-body">
              <span>Twitter</span>
              <a href="javascript:;">{{userData()->twitter_acc}}</a>
            </div>
          </div><!-- media -->
          <div class="media">
            <div class="media-icon"><i class="icon ion-logo-linkedin"></i></div>
            <div class="media-body">
              <span>Linkedin</span>
              <a href="javascript:;">{{userData()->linkedin_acc}}</a>
            </div>
          </div><!-- media -->
          <div class="media">
            <div class="media-icon"><i class="icon ion-md-link"></i></div>
            <div class="media-body">
              <span>My Portfolio</span>
              <a href="javascript:;">{{userData()->my_acc}}</a>
            </div>
          </div><!-- media -->
          <div class="media">
            <div class="media-icon"><i class="fa fa-globe"></i></div>
            <div class="media-body">
              <span>My Website</span>
              <a href="javascript:;">{{userData()->website}}</a>
            </div>
          </div><!-- media -->
        </div><!-- az-profile-social-list -->

      </div><!-- az-profile-overview -->

    </div><!-- az-content-left -->
    <div class="az-content-body az-content-body-profile col-lg-9">
      <nav class="nav az-nav-line">
        <a href="" class="nav-link active" data-toggle="tab">Overview</a>
        <a href="" class="nav-link" data-toggle="tab">Reviews</a>
        <a href="" class="nav-link" data-toggle="tab">Followers</a>
        <a href="" class="nav-link" data-toggle="tab">Following</a>
        <a href="" class="nav-link" data-toggle="tab">Account Settings</a>
      </nav>

      <div class="az-profile-body">

        <div class="row mg-b-20">
          <div class="col-md-7 col-xl-8">
            <div class="az-profile-view-chart">
              <canvas id="chartArea"></canvas>
              <div class="az-profile-view-info">
                <div class="d-flex align-items-baseline">
                  <h6>508</h6>
                  <span>-1.2% since last week</span>
                </div>
                <p>Profile viewers past 10 days</p>
              </div>
            </div>
          </div>
          <div class="col-md-5 col-xl-4 mg-t-40 mg-md-t-0">
            <div class="az-content-label tx-13 mg-b-20">
              Skill Sets
              <a href="javascript:;" class="text-info float-right" onclick="loadPage('loadedPage','0','{{ url('profileskillsform') }}');$('.az-content-profile').hide();">
                <i class="fa fa-edit"></i>
              </a>
            </div>

            @if (!empty($skills))
              @foreach ($skills as $skill)
                <div class="az-traffic-detail-item">
                  <div>
                    <span>{{$skill->name}}</span>
                    <span>{{$skill->number}}%</span>
                  </div>
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="{{$skill->number}}" aria-valuemin="0" aria-valuemax="100" style="background-color:{{$skill->bgcolor}}; width: {{$skill->number}}%"></div>
                  </div>
                </div>
              @endforeach
              @else
              <h2 class="text-secondary">Not Available</h2>
            @endif

          </div>
        </div><!-- row -->

        <hr class="mg-y-40">

        <div class="row">
          <div class="col-md-7 col-xl-8">
            <div class="az-content-label tx-13 mg-b-25">Work &amp; Education</div>
            
            <div class="az-profile-work-list">

              @if (!empty($workingExp))
                @foreach ($workingExp as $work)
                  <div class="media">
                    <div class="media-logo bg-success"><i class="fas fa-building"></i></i></i></div>
                    <div class="media-body">
                      <h6>{{$work->designation}} at <a href="{{$work->website}}" target="_blank">{{$work->organization}}</a></h6>
                      <span>{{$work->from}} - {{$work->to}}</span>
                    </div>
                  </div>
                @endforeach
                @else
                <h2 class="text-secondary">Not Available</h2>
              @endif

              @if (!empty($educations))
                @foreach ($educations as $education)
                  <div class="media">
                    <div class="media-logo bg-primary"><i class="fas fa-graduation-cap"></i></i></div>
                    <div class="media-body">
                      <h6>{{$education->name}} from <a href="javascript:;">{{$education->university}}</a></h6>
                      <span>{{$education->from}} - {{$education->to}}</span>
                    </div>
                  </div>
                @endforeach
                @else
                <h2 class="text-secondary">Not Available</h2>
              @endif

            </div>

          </div>
          <div class="col-md-5 col-xl-4 mg-t-40 mg-md-t-0">
            <div class="az-content-label tx-13 mg-b-25">Contact Information</div>
            <div class="az-profile-contact-list">
              <div class="media">
                <div class="media-icon"><i class="icon ion-md-phone-portrait"></i></div>
                <div class="media-body">
                  <span>Mobile</span>
                  <div>{{userData()->phone}}</div>
                </div><!-- media-body -->
              </div><!-- media -->
              <div class="media">
                <div class="media-icon"><i class="icon ion-logo-slack"></i></div>
                <div class="media-body">
                  <span>Slack</span>
                  <div>{{userData()->slack_acc}}</div>
                </div><!-- media-body -->
              </div><!-- media -->
              <div class="media">
                <div class="media-icon"><i class="icon ion-md-locate"></i></div>
                <div class="media-body">
                  <span>Current Address</span>
                  <div>{{userData()->address1}}</div>
                </div><!-- media-body -->
              </div><!-- media -->
            </div><!-- az-profile-contact-list -->
          </div>
        </div><!-- row -->

        <div class="mg-b-20"></div>

      </div><!-- az-profile-body -->
    </div><!-- az-content-body -->
  </div><!-- container -->
</div>

<script src="{{ asset('assets/lib/chart.js/Chart.bundle.min.js')}}"></script>
<script>
  $(function(){
    'use strict'

    /** AREA CHART **/
    var ctx = document.getElementById('chartArea').getContext('2d');

    var gradient = ctx.createLinearGradient(0, 240, 0, 0);
    gradient.addColorStop(0, 'rgba(0,123,255,0)');
    gradient.addColorStop(1, 'rgba(0,123,255,.3)');

    new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['Oct 1', 'Oct 2', 'Oct 3', 'Oct 4', 'Oct 5', 'Oct 6', 'Oct 7', 'Oct 8', 'Oct 9', 'Oct 10'],
        datasets: [{
          data: [12, 15, 18, 40, 35, 38, 32, 20, 25],
          borderColor: '#007bff',
          borderWidth: 1,
          backgroundColor: gradient
        }]
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: false,
            labels: {
              display: false
            }
        },
        scales: {
          yAxes: [{
            display: false,
            ticks: {
              beginAtZero:true,
              fontSize: 10,
              max: 80
            }
          }],
          xAxes: [{
            ticks: {
              beginAtZero:true,
              fontSize: 11,
              fontFamily: 'Arial'
            }
          }]
        }
      }
    });

  });
</script>

<script>
  function loadPage(viewId,pageId,url){
    $("#"+viewId).load(url+'/'+pageId);
  }

  $(document).ready(function(){
    $('#userProfileInput').change(function(){
      event.preventDefault();
      $.ajax({
        url:"{{'uploadImageUser'}}"+"/{{userData()->id}}",
        method:"POST",
        data:new FormData(document.getElementById("profileUploadForm")),
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success:function(data){
          $('.alert').hide(200);
          var html='<div class="alert '+ data.class_name +' mg-b-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+ data.message +'</strong></div>';
          $( ".az-content-contacts" ).prepend($(html));
          $("#uploaded_image").attr("src",data.imagePath);
        }
      })
    });
  });
</script>

@endsection