@extends('layouts.app')
@section('content')

  <div class="az-content az-content-app az-content-contacts pd-b-0">

    @if(session('success'))
      <div class="alert alert-success mg-b-0" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>{{session('success')}}</strong>
      </div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger mb-0">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <strong>Error!</strong> 
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <div class="container-fluid">
      <div class="az-content-left az-content-left-contacts">
        <div class="az-content-breadcrumb lh-1 mg-b-10">
          <span>Apps &amp; Pages</span>
          <span>Staff</span>
        </div>
        <h2 class="az-content-title tx-24 mg-b-30">My Staff
          @if(auth()->user()['is_admin'] == 1)
            <a href="javascript:;" class="text-info text-right ml-2 tx-20" onclick="loadPage('loadedPage','0','{{ url('saveuserform') }}');"><i class="fa fa-user-plus"></i></a>
          @endif
        </h2>
        <nav class="nav az-nav-line az-nav-line-chat">
          <a href="" data-toggle="tab" class="nav-link active">All Staff</a>
          <a href="" data-toggle="tab" class="nav-link">Groups</a>
        </nav>
        <div id="azContactList" class="az-contacts-list">
          <div class="az-contact-label">A</div>

          @foreach($allUsers as $user)
            <div class="az-contact-item @if($user->id==1){{'selected'}}@endif" onclick="loadPage('loadedPage','{{$user->id}}','{{ url('staff_details') }}');" data-id="{{$user->id}}">
              <div class="az-img-user online">

                @if(!empty($user->profile) && file_exists( public_path().'/images/users/'.$user->profile ))
                  <img src="{{'images/users/'.$user->profile}}">
                @else
                  <img src="{{'images/user.png'}}">
                @endif

              </div>
              <div class="az-contact-body">
                <h6>{{ $user->name }}</h6>
                <span class="phone">{{ $user->phone }}</span>
              </div>
              <a href="" class="az-contact-star active"><i class="typcn typcn-star"></i></a>
            </div>
          @endforeach

          <div class="az-contact-label">B</div>
        </div>
      </div>

      <div id="loadedPage" style="width: 100%"><x-staffdetails /></div>

    </div>
  </div>

  <script>
    function loadPage(viewId,pageId,url){
      $("#"+viewId).load(url+'/'+pageId);
    }
  </script>

  <script>
    $(function(){
      'use strict'

      new PerfectScrollbar('#azContactList', {
        suppressScrollX: true
      });

      new PerfectScrollbar('.az-contact-info-body', {
        suppressScrollX: true
      });

      $('.az-contact-item').on('click touch', function() {
        $(this).addClass('selected');
        $(this).siblings().removeClass('selected');

        $('body').addClass('az-content-body-show');
      })

    });
  </script>

@endsection