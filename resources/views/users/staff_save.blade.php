<link rel="stylesheet" href="{{ asset('assets/lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">

<div class="az-content-body az-content-body-contacts">
	<form action="@if(!empty($userDetails)) updateUserDetails @else saveUserDetails @endif" method="post" data-parsley-validate>
		{{@csrf_field()}}

		@if(!empty($userDetails->name)) 
			<input type="hidden" name="id" value="{{$userDetails->id}}">
		@endif

		<div class="row">
			<div class="col-md-12 dis-flex">
				<span class="az-content-title">@if(!empty($userDetails)) Edit @else Add @endif</span>
				<div id="slWrapper" class="parsley-select mg-l-10">
					<select name="designation" class="form-control select2" data-placeholder="Choose Designation" data-parsley-class-handler="#slWrapper" data-parsley-errors-container="#slErrorContainer" required>
						<option label="Choose Designation"></option>
						@foreach(userDesignations() as $userDesignation)
							@if (!empty($userDetails->designation) && $userDesignation['designation_name'] == $userDetails['designation'])
								<option value="{{$userDesignation['designation_name']}}" selected>{{$userDesignation['designation_name']}}</option>
							@else
								<option value="{{$userDesignation['designation_name']}}">{{$userDesignation['designation_name']}}</option>
							@endif
						@endforeach
					</select>
					<div id="slErrorContainer"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="group">
					<input type="text" name="name" class="form-control" value="@if(!empty($userDetails->name)) {{$userDetails->name}} @endif" required>
					<span class="highlight"></span>
					<label>Name</label>
				</div>

				<div class="group form-group">
					<input type="email" name="email" class="form-control" value="@if(!empty($userDetails->email)) {{$userDetails->email}} @endif" required>
					<span class="highlight"></span>
					<label>Email</label>
				</div>

				<div class="group form-group">
					<input type="password" name="password" class="form-control" @if(empty($userDetails)) required @endif>
					<span class="highlight"></span>
					<label>Password</label>
				</div>

				<div class="group form-group">
					<input type="password" name="c_password" class="form-control" @if(empty($userDetails)) required @endif>
					<span class="highlight"></span>
					<label>Confirm Password</label>
				</div>

				<div class="group form-group">
					<input type="text" name="other_acc" class="form-control" value="@if(!empty($userDetails->other_acc)) {{$userDetails->other_acc}} @endif" required>
					<span class="highlight"></span>
					<label>Other Account</label>
				</div>
			</div>

			<div class="col-md-6">
				<div class="group form-group">
					<input type="text" name="phone" class="form-control phoneMask" value="@if(!empty($userDetails->phone)) {{$userDetails->phone}} @endif" required>
					<span class="highlight"></span>
					<label>Work Contact</label>
				</div>

				<div class="group form-group">
					<input type="text" name="phone_home" class="form-control phoneMask" value="@if(!empty($userDetails->phone_home)) {{$userDetails->phone_home}} @endif" required>
					<span class="highlight"></span>
					<label>Personal Contact</label>
				</div>

				<div class="group form-group">
					<label class="label-textarea">Current Address</label>
					<textarea name="address1" rows="3" class="form-control" placeholder="Type Here" required>@if(!empty($userDetails->address1)) {{$userDetails->address1}} @endif</textarea>
				</div>

				<div class="group form-group">
					<label class="label-textarea">Permanent Address</label>
					<textarea name="address2" rows="3" class="form-control" placeholder="Type Here" required>@if(!empty($userDetails->address2)) {{$userDetails->address2}} @endif</textarea>
				</div>
			</div>
		</div>

		<button type="submit" class="btn btn-dark pd-x-20">Submit</button>
	</form>
</div>

<script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/lib/parsleyjs/parsley.min.js') }}"></script>
<script src="{{ asset('assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}"></script>

<script>
	$(function(){
		'use strict'
		$(document).ready(function(){
			$('.select2').select2({
				placeholder: 'Choose Designation'
			});

			$('.select2-no-search').select2({
				minimumResultsForSearch: Infinity,
				placeholder: 'Choose Designation'
			});
		});
		$('#selectForm').parsley();
		$('#selectForm2').parsley();

		$('.phoneMask').mask('(999) 999-9999');
	});
</script>