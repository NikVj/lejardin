<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::group(['middleware'=>['auth']],
	function(){

		Route::view('app/calendar', 'calendar/calendar');
		Route::view('app/messaging', 'communique/chat');
		Route::view('app/mail', 'communique/mail');

		Route::get('staff/{id?}', 'App\Http\Controllers\UsersController@index');
		Route::get('saveuserform/{id?}', 'App\Http\Controllers\UsersController@saveUserForm');
		Route::get('staff_details/{id?}', 'App\View\Components\staffdetails@render');
		Route::post('saveUserDetails/{id?}', 'App\Http\Controllers\UsersController@saveUserDetails');
		Route::post('updateUserDetails/{id?}', 'App\Http\Controllers\UsersController@updateUserDetails');
		Route::get('deleteUser/{id?}', 'App\Http\Controllers\UsersController@deleteUser');
		Route::post('uploadImageUser/{id?}','App\Http\Controllers\UsersController@uploadImageUser');

		Route::get('profile/{id?}', 'App\Http\Controllers\ProfileController@index');
		Route::get('profileform/{id?}', 'App\Http\Controllers\ProfileController@profileForm');
		Route::get('profileskillsform/{id?}', 'App\Http\Controllers\ProfileController@profileSkillsForm');
		Route::post('updateUserProfile/{id?}', 'App\Http\Controllers\ProfileController@edit');
		Route::post('updateUserSkills/{id?}', 'App\Http\Controllers\ProfileController@updateUserSkills');
		
		Route::post('app/addCalDetails/{id?}', 'App\Http\Controllers\CalendarController@addCalDetails');
		Route::get('app/deleteCalendar/{id?}', 'App\Http\Controllers\CalendarController@deleteCalDetails');
	}
);